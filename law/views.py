import datetime
import json
from zipfile import ZipFile

from io import BytesIO

import requests
from django.http import JsonResponse, HttpResponse, HttpResponseForbidden, HttpResponseNotFound, FileResponse, \
    HttpResponseBadRequest
from django.shortcuts import redirect


def index(request):
    return redirect('http://infralabs.cs.ui.ac.id:20061/')


def calculate_time(request):
    if request.method == 'GET':
        time = datetime.datetime.today()

        hours = request.GET.get('hours')
        time = time + datetime.timedelta(hours=int(hours)) if hours is not None else time

        minutes = request.GET.get('minutes')
        time = time + datetime.timedelta(minutes=int(minutes)) if minutes is not None else time

        seconds = request.GET.get('seconds')
        time = time + datetime.timedelta(seconds=int(seconds)) if seconds is not None else time

        response = {
            'current': datetime.datetime.today().strftime('%H:%M:%S'),
            'hours': time.hour,
            'minutes': time.minute,
            'seconds': time.second
        }
        return JsonResponse(response)
    else:
        return HttpResponseNotFound()


def arithmetic(request):
    available_op = ['add', 'sub', 'mult', 'div', 'pow']
    if request.method == 'GET':
        if 'op' in request.GET and 'a' in request.GET and 'b' in request.GET:
            op = request.GET['op']
            try:
                a = int(request.GET['a'])
                b = int(request.GET['b'])
            except ValueError:
                return HttpResponseBadRequest()
            if op in available_op:
                response = {
                    'a': a,
                    'b': b,
                    'op': op
                }
                if op == 'add':
                    response['result'] = a + b
                    return JsonResponse(response)
                elif op == 'sub':
                    response['result'] = a - b
                    return JsonResponse(response)
                elif op == 'mult':
                    response['result'] = a * b
                    return JsonResponse(response)
                elif op == 'div':
                    response['result'] = a / b
                    return JsonResponse(response)
                elif op == 'pow':
                    response['result'] = a ** b
                    return JsonResponse(response)
                else:
                    return HttpResponseBadRequest()
            else:
                return HttpResponseBadRequest()
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseNotFound()


def zip_file(request):
    if request.method == 'POST':
        if 'Authorization' in request.headers:
            headers = {'Authorization': request.headers['Authorization']}
            oauth_request = requests.get('http://oauth.infralabs.cs.ui.ac.id/oauth/resource', headers=headers)
            oauth_response = oauth_request.json()
            if 'error' not in oauth_response:
                in_memory = BytesIO()
                compressed = ZipFile(in_memory, "a")
                archive = request.FILES.get('file')
                compressed.writestr(archive.name, archive.file.read())
                for file in compressed.filelist:
                    file.create_system = 0
                compressed.close()
                response = HttpResponse(content_type='application/zip')
                response['Content-Disposition'] = f'attachment; filename={archive.name}.zip'
                in_memory.seek(0)
                response.write(in_memory.read())
                return response
            else:
                return HttpResponseForbidden(json.dumps(oauth_response), content_type='application/json')
        else:
            return HttpResponseForbidden('Authorization header required')
    else:
        return HttpResponseNotFound()
