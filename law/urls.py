from django.urls import path

from law.views import calculate_time, zip_file, arithmetic

urlpatterns = [
    path('time-calc', calculate_time),
    path('compress', zip_file),
    path('arithmetic', arithmetic)
]
